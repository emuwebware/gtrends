'use strict';
const express = require('express')
const app = express()
const port = 3000

const googleTrends = require('google-trends-api');

app.get('/', (req, res) => {
  
  googleTrends.interestOverTime({
    keyword: '/m/07mzm6',
    // startTime: new Date(Date.now() - (1 * 60 * 60 * 1000)),
    // startTime: new Date(Date.now() - (1 * 60 * 60 * 1000)),
    startTime: new Date('November 4, 2016 00:00:00'),
    endTime: new Date('December 1, 2016 00:00:00'),
    // geo: 'AU-VIC',
    geo: 'AU-VIC',
    // geo: 'AU-20040',
    // geo: '20040',
    // category: 45 // health
  })
  .then((result) => {
    res.send(result)
    // console.log('this is res', res);
  })
  .catch((err) => {
    console.log('got the error', err);
    console.log('error message', err.message);
    console.log('request body',  err.requestBody);
  });

})



app.listen(port, () => console.log(`Example app listening on port ${port}!`))